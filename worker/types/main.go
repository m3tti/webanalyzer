package types

import "github.com/chromedp/cdproto/network"

type AllData struct {
	Url    string   `json:"url"`
	Events []*Event `json:"events"`
	Html   string   `json:"html"`
}

type Event struct {
	Name    string       `json:"name"`
	Payload *interface{} `json:"payload"`
}

type Data struct {
	Url       string                            `json:"url"`
	Requests  []*network.EventRequestWillBeSent `json:"requests"`
	Responses []*network.EventResponseReceived  `json:"responses"`
	Image     string                            `json:"image"`
	VT        []*VT                             `json:"vt"`
}

type VT struct {
	Url   string `json:"url"`
	VTUrl string `json:"vt_url"`
	Sha1  string `json:"sha1"`
	Page  string `json:"page"`
}
