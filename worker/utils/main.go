package utils

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"gitlab/m3tti/webAnalyzer/worker/types"
	"io/ioutil"
	"log"
	"net/http"
)

func WriteJson(data *types.Data) {
	bytes, err := json.Marshal(data)

	if err != nil {
		panic(err)
	}

	if err := ioutil.WriteFile("data.json", bytes, 0644); err != nil {
		log.Fatal(err)
	}
}

func Sha1(str string) string {
	h := sha256.New()

	h.Write([]byte(str))
	bs := h.Sum(nil)

	return fmt.Sprintf("%x", bs)
}

func GetVT(url string) *types.VT {
	sha1 := Sha1(url)
	vturl := "https://www.virustotal.com/old-browsers/url/" + sha1
	resp, err := http.Get(vturl)

	if err != nil {
		return nil
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil
	}

	page := string(body)
	return &types.VT{
		Url:   url,
		VTUrl: vturl,
		Sha1:  sha1,
		Page:  page,
	}
}
