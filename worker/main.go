package main

import (
	"gitlab/m3tti/webAnalyzer/worker/worker"

	"github.com/gin-gonic/gin"
)

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Allow-Methods", "POST,HEAD,PATCH, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func main() {
	r := gin.Default()

	r.Use(CORSMiddleware())

	r.GET("/", func(c *gin.Context) {
		url := c.Query("url")
		data := worker.Analyze(url)

		c.JSON(200, data)
	})

	r.GET("/all", func(c *gin.Context) {
		url := c.Query("url")
		data := worker.AnalyzeAll(url)

		c.JSON(200, data)
	})

	r.Run(":8080")
}
