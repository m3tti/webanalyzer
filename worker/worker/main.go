package worker

import (
	"context"
	"encoding/base64"
	"fmt"
	"gitlab/m3tti/webAnalyzer/worker/types"
	"gitlab/m3tti/webAnalyzer/worker/utils"
	"reflect"
	"time"

	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/chromedp"
)

func AnalyzeAll(url string) *types.AllData {
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	var data []*types.Event

	chromedp.ListenBrowser(ctx, func(ev interface{}) {
		data = append(data, &types.Event{
			Name:    fmt.Sprintf("%s", reflect.TypeOf(ev)),
			Payload: &ev,
		})
	})

	chromedp.ListenTarget(ctx, func(ev interface{}) {
		data = append(data, &types.Event{
			Name:    fmt.Sprintf("%s", reflect.TypeOf(ev)),
			Payload: &ev,
		})
	})

	// navigate
	if err := chromedp.Run(ctx, chromedp.Navigate(url)); err != nil {
		fmt.Errorf("could not navigate to github: %v", err)
	}

	return &types.AllData{
		Url:    url,
		Events: data,
		Html:   "html",
	}
}

func Analyze(url string) *types.Data {
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	requests := []*network.EventRequestWillBeSent{}
	responses := []*network.EventResponseReceived{}

	chromedp.ListenBrowser(ctx, func(ev interface{}) {
		fmt.Printf("Browser event %s\n", reflect.TypeOf(ev))
	})

	chromedp.ListenTarget(ctx, func(ev interface{}) {
		if v, ok := ev.(*network.EventRequestWillBeSent); ok {
			requests = append(requests, v)
		}

		if v, ok := ev.(*network.EventResponseReceived); ok {
			responses = append(responses, v)
		}
	})

	// navigate
	if err := chromedp.Run(ctx, chromedp.Navigate(url)); err != nil {
		fmt.Errorf("could not navigate to github: %v", err)
	}

	encodedImage := screenshot(&ctx, time.Now().String())
	return &types.Data{
		Url:       url,
		Requests:  requests,
		Responses: responses,
		Image:     encodedImage,
		VT:        getVT(requests),
	}
}

func screenshot(ctx *context.Context, time string) string {
	var buf []byte
	if err := chromedp.Run(*ctx, chromedp.FullScreenshot(&buf, 90)); err != nil {
		fmt.Errorf("could not navigate to github: %v", err)
	}

	return base64.StdEncoding.EncodeToString([]byte(buf))
}

func getVT(requests []*network.EventRequestWillBeSent) []*types.VT {
	var vt []*types.VT

	for _, req := range requests {
		vt = append(vt, utils.GetVT(req.Request.URL))
	}

	return vt
}
