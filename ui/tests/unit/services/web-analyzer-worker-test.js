import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Service | web-analyzer-worker', function (hooks) {
  setupTest(hooks);

  // TODO: Replace this with your real tests.
  test('it exists', function (assert) {
    let service = this.owner.lookup('service:web-analyzer-worker');
    assert.ok(service);
  });
});
