import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | analysis/details/index', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:analysis/details/index');
    assert.ok(route);
  });
});
