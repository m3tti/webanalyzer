import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | analysis/details/urls', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:analysis/details/urls');
    assert.ok(route);
  });
});
