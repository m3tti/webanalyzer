import { helper } from '@ember/component/helper';

export default helper(function shorten(positional /*, named*/) {
  if (positional[0].length > 100) {
    return (
      positional[0].substring(0, 50) +
      ' ... ' +
      positional[0].substring(positional[0].length - 50)
    );
  }
  return positional[0];
});
