import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class IpComponent extends Component {
  @tracked open;

  @action
  toggle() {
    this.open = !this.open;
  }
}
