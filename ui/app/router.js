import EmberRouter from '@ember/routing/router';
import config from 'ui/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('analysis', function () {
    this.route('details', function () {
      this.route('urls');
      this.route('ips');
      this.route('events');
    });
  });
});
