import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class AnalysisDetailsRoute extends Route {
  @service webAnalyzerWorker;

  queryParams = {
    url: {
      refreshModel: true,
    },
  };

  async model(params) {
    return new Promise((resolve) =>
      Promise.all([
        this.webAnalyzerWorker.getAnalysis(params.url),
        this.webAnalyzerWorker.getAnalysisEvents(params.url),
      ]).then((values) => {
        resolve({
          urls: values[0],
          events: values[1],
        });
      })
    );
  }
}
