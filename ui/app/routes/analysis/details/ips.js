import Route from '@ember/routing/route';
import { groupBy, path } from 'ramda';

export default class AnalysisDetailsIpsRoute extends Route {
  model() {
    let model = this.modelFor('analysis.details');

    return groupBy(path(['response', 'remoteIPAddress']), model.urls.responses);
  }
}
