import Route from '@ember/routing/route';

export default class AnalysisDetailsUrlsRoute extends Route {
  model() {
    return this.modelFor('analysis.details');
  }
}
