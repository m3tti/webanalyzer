import Route from '@ember/routing/route';

export default class AnalysisDetailsEventsRoute extends Route {
  model() {
    let model = this.modelFor('analysis.details');

    return model.events;
  }
}
