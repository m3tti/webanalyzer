import Service from '@ember/service';
import fetch from 'fetch';
import { map, filter } from 'ramda';

export default class WebAnalyzerWorkerService extends Service {
  ENDPOINT = 'http://localhost:8080/?url=';
  ENDPOINT_ALL = 'http://localhost:8080/all?url=';
  url = '';
  data = {};

  async getAnalysis(url) {
    if (url === this.url) {
      return this.data;
    }

    return fetch(this.ENDPOINT + url)
      .then((response) => response.json())
      .then((d) => {
        this.data = {
          ...d,
          ...this.mergedData(d),
        };
        return this.data;
      });
  }

  async getAnalysisEvents(url) {
    if (url === this.url) {
      return data;
    }

    return fetch(this.ENDPOINT_ALL + url)
      .then((response) => response.json())
      .then((d) => {
        this.data = d;
        return this.data;
      });
  }

  mergedData(d) {
    let filterResponsesByRequestId = (reqId) =>
      filter((res) => {
        return res.requestId == reqId;
      }, d.responses);

    let attachedResponse = map((req) => {
      return {
        ...req,
        responses: filterResponsesByRequestId(req.requestId),
      };
    }, d.requests);

    return {
      mergedData: attachedResponse,
    };
  }
}
